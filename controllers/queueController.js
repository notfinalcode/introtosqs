var AWS = require('aws-sdk');

var QueueController = {
    queueName: "NFC-Tutorial-Queue",
    queueAttributes: {
        MessageRetentionPeriod: "300", // 5 minutes
        ReceiveMessageWaitTimeSeconds: "1",
        VisibilityTimeout: "10" //
    },
    queueParams: {},

    getSqs: function() {
        AWS.config.loadFromPath('./config/aws.settings');
        this.queueParams = {
            QueueName: this.queueName,
            Attributes: this.queueAttributes
        };
        return new AWS.SQS({
            apiVersion: '2012-11-05'
        });
    },

    listQueues: function(callback) {
        var sqs = this.getSqs();

        sqs.listQueues({}, function(err, data) {
            if(err) {
                console.log(err, err.stack);
            } else {
                console.log(data);
                if(typeof(callback) == "function")
                {
                    callback(data);
                }
            }
        });

    },

    createQueue: function(callback) {

        var sqs = this.getSqs();

        var controller = this;

        console.log(this.queueParams);

        sqs.createQueue(this.queueParams, function(err, data) {
            if (err) {
                if (err.code == "QueueAlreadyExists") {
                    // get the queue URL by ignoring the attributes:
                    var getQueueParams = {
                        QueueName: controller.queueName
                    };

                    sqs.getQueueUrl(getQueueParams, function (err2, data2) {
                        if (err2) {
                            console.log(err2, err2.stack);
                        } else {
                            console.log(data2);

                            // now set the attributes:
                            var setQueueAttribsParams = {
                                QueueUrl: data2.QueueUrl,
                                Attributes: controller.queueAttributes
                            };

                            sqs.setQueueAttributes(setQueueAttribsParams, function (err3, data3) {
                                if (err3) {
                                    console.log(err3, err3.stack);
                                } else {
                                    console.log(data3);
                                    if (typeof(callback) == "function") {
                                        callback(data3);
                                    }
                                }
                            });

                        }
                    });
                } else {
                    console.log(err, err.stack);
                }
            }
            else
            {
                console.log(data);
                if(typeof(callback) == "function")
                {
                    callback(data);
                }
            }

        });

    },

    sendSimpleMessage:function(messageText, callback) {
        var sqs = this.getSqs();

        this.createQueue(function (data) {
            if (data.QueueUrl) {
                var params = {
                    MessageBody: messageText,
                    QueueUrl: data.QueueUrl
                };
                console.log("Sending Message");

                sqs.sendMessage(params, function (err, data) {
                    if (err) {
                        console.log(err, err.stack);
                    }
                    else {
                        console.log(data);
                        if(typeof(callback) == "function")
                        {
                            callback(data);
                        }
                    }
                });
            } else {
                console.log(data);
            }
        })
    },

    sendEncodeRequest:function(originalName, newName, bitRate, callback) {
        var sqs = this.getSqs();
        var messageBody = "encodeTrack";

        this.createQueue(function (data) {
            if (data.QueueUrl) {
                var params = {
                    MessageBody: messageBody,
                    QueueUrl: data.QueueUrl,
                    MessageAttributes: {
                        OriginalName: {
                            DataType: "String",
                            StringValue: originalName
                        },
                        NewName: {
                            DataType: "String",
                            StringValue: newName
                        },
                        BitRate: {
                            DataType: "Number",
                            StringValue: bitRate
                        }
                    }
                };
                console.log("Sending Message");

                sqs.sendMessage(params, function (err, data) {
                    if (err) {
                        console.log(err, err.stack);
                    }
                    else {
                        console.log(data);
                        if(typeof(callback) == "function")
                        {
                            callback(data);
                        }
                    }
                });
            } else {
                console.log(data);
            }
        })
    },

    receiveSingleMessage: function(callback) {
        var sqs = this.getSqs();
        var controller = this;

        this.createQueue(function (data) {
            var params = {
                QueueUrl: data.QueueUrl,
                MaxNumberOfMessages: 1,
                MessageAttributeNames: [
                    "OriginalName", "NewName", "BitRate"
                ]
            };


            sqs.receiveMessage(params, function(err, data) {
                if (err) {
                    console.log(err, err.stack);
                }
                else {
                    console.log(data);
                    if(data.Messages.length > 0 && data.Messages[0].Body == "encodeTrack")
                    {
                        controller.handleEncodeRequest(data.Messages[0], callback);
                    }
                    else if(typeof(callback) == "function")
                    {
                        callback(data);
                    }

                    if(data.Messages.length > 0)
                    {
                        controller.deleteMessage(sqs, data.Messages[0].ReceiptHandle, params.QueueUrl);
                    }
                }
            });
        });
    },

    handleEncodeRequest: function(message, callback) {

        var OriginalName = "", NewName = "", BitRate = "";

        if(typeof(message.MessageAttributes.OriginalName !== 'undefined'))
        {
            OriginalName = message.MessageAttributes.OriginalName.StringValue;
        }

        if(typeof(message.MessageAttributes.NewName !== 'undefined'))
        {
            NewName = message.MessageAttributes.NewName.StringValue;
        }

        if(typeof(message.MessageAttributes.BitRate !== 'undefined'))
        {
            BitRate = message.MessageAttributes.BitRate.StringValue;
        }

        /* Here is where we'd actually handle the message */

        if(typeof(callback) == "function")
        {
            callback({
                OriginalName: OriginalName,
                NewName: NewName,
                BitRate: BitRate
            })
        }

    },

    deleteMessage: function(sqs, handle, queueUrl) {
        var params = {
            QueueUrl: queueUrl,
            ReceiptHandle: handle
        };

        sqs.deleteMessage(params, function(err, data) {
            if (err) {
                console.log(err, err.stack);
            }
            else {
                console.log(data);
            }
        });
    }

};

module.exports = QueueController;