var express = require('express'),
    cors = require('cors'),
    bodyParser = require('body-parser');

var queueController = require('./controllers/queueController');

var app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.get('/', function (req, res) {
    res.send('Hello, you');
});

app.get('/listQueues', function(req, res) {
    queueController.listQueues(function(data) {
        res.send(JSON.stringify(data));
    });
});

app.post('/createQueue', function(req, res) {
    queueController.createQueue(function (data) {
        res.send(JSON.stringify(data));
    });
});

app.post('/sendMessage', function(req, res) {
    var message = req.body.message;

    queueController.sendSimpleMessage(message, function(data) {
        res.send(JSON.stringify(data));
    });
});

app.post('/sendEncodeRequest', function(req, res) {
    var oldTitle = req.body.OldTitle;
    var newTitle = req.body.NewTitle;
    var bitRate = req.body.BitRate;

    queueController.sendEncodeRequest(oldTitle, newTitle, bitRate, function(data) {
        res.send(JSON.stringify(data));
    });
});

app.get('/receiveMessage', function (req, res) {
    queueController.receiveSingleMessage(function(data) {
        res.send(JSON.stringify(data));
    })
});

app.listen(3030, function() {
    console.log("Listening on port 3030...");
});